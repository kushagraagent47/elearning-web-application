require('dotenv').config();
var express = require("express");
var app = express();
var bodyParser = require("body-parser");
var session = require("express-session");
const request = require('request');
var cookieParser = require("cookie-parser");
var flash = require("req-flash");
var mysql = require("mysql");
var nodemailer = require("nodemailer");
const qs = require('querystring');
const randomString = require('randomstring');
const axios = require('axios');
var hbs = require("nodemailer-express-handlebars");
var crypto = require("crypto");
var generator = require('generate-password');
var port = 8000;
var router = express.Router();
app.set("view engine", "ejs");
app.use(
  bodyParser.urlencoded({
    extended: false
  })
);
app.use(cookieParser());

app.use(express.static(__dirname + '/public'));

// OAUTH CONFIG
const redirect_uri = process.env.HOST + '/redirect';


// Express session
app.use(
  session({
    secret: randomString.generate(),
    resave: false,
    saveUninitialized: true
  })
);

app.use(flash());

//DB CONNECTION
var con = mysql.createConnection({
  host: process.env.port,
  user: "root",
  port: "3306",
  password: process.env.pass,
  database: "elearn"
});

con.connect(function (err) {
  if (err) throw err;
  console.log("Connected!");
});

//Mail Config
var transporter = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: "codeleat@gmail.com",
    pass: process.env.password
  }
});

const handlebarOptions = {
  viewEngine: {
    extName: '.hbs',
    partialsDir: 'partials/',
    layoutsDir: 'public/',
    defaultLayout: 'signup.hbs',
  },
  viewPath: 'public/',
  extName: '.hbs',
};

transporter.use('compile', hbs(handlebarOptions));

// Index or dashboard page
app.get("/", function (req, res) {
  if (req.session.type == "pro") {
    con.query(
      "SELECT * FROM courses WHERE email = '" + req.session.email + "'",
      function (err, result, fields) {
        if (err) throw err;
        res.render("admin_dashboard", {
          email: req.session.email,
          courses: result,
          CourseAdded: req.flash("CourseAdded")
        });
      }
    );
  } else if (req.session.type == "student") {
    con.query(
      "SELECT * FROM courses WHERE id IN(SELECT lesson_id FROM subscriptions WHERE user_email = '" +
      req.session.email +
      "')",
      function (err, my_courses, fields_courses) {
        con.query(
          "SELECT * FROM courses WHERE id NOT IN(SELECT lesson_id FROM subscriptions WHERE user_email = '" +
          req.session.email +
          "')",
          function (err, result_courses, fields_courses) {
            con.query(
              "SELECT lessons.course_id, lessons.title, lessons.course_title, courses.id, courses.email, lessons.lesson FROM lessons LEFT OUTER JOIN courses ON lessons.course_id = courses.id WHERE lessons.course_id IN (SELECT lesson_id FROM subscriptions WHERE user_email = '" +
              req.session.email +
              "') ORDER BY lessons.id DESC",
              function (err, result, fields) {
                if (err) throw err;
                res.render("feed", {
                  email: req.session.email,
                  feed: result,
                  unsubscribed_courses: result_courses,
                  my_courses: my_courses
                });
              }
            )
          });
      });
  }

  else {
    res.render("index");
  }
});

// Signup Page
app.get("/signup", function (req, res) {
  var password = generator.generate({
    length: 15,
    numbers: true
  });
  res.render("signup", {
    random_pass: password
  });
});

// GITHUB LOGIN
app.get('/login/github', (req, res, next) => {
  req.session.csrf_string = randomString.generate();
  const githubAuthUrl =
    'https://github.com/login/oauth/authorize?' +
    qs.stringify({
      client_id: process.env.CLIENT_ID,
      redirect_uri: redirect_uri,
      state: req.session.csrf_string,
      scope: 'user:email'
    });
  res.redirect(githubAuthUrl);
});

app.all('/redirect', (req, res) => {
  const code = req.query.code;
  const returnedState = req.query.state;
  if (req.session.csrf_string === returnedState) {
    request.post(
      {
        url:
          'https://github.com/login/oauth/access_token?' +
          qs.stringify({
            client_id: process.env.CLIENT_ID,
            client_secret: process.env.CLIENT_SECRET,
            scope: 'user:email',
            code: code,
            redirect_uri: redirect_uri,
            state: req.session.csrf_string
          })
      },
      (error, response, body) => {
        req.session.access_token = qs.parse(body).access_token;
        res.redirect('/user');
      }
    );
  } else {
    res.redirect('/');
  }
});

app.get('/user', (req, res) => {
  request.get(
    {
      url: 'https://api.github.com/user',
      headers: {
        Authorization: 'token ' + req.session.access_token,
        'User-Agent': 'node.js',
        "Content-Type": "application/json"
      }
    },
    (error, response, body) => {
      var user_data = JSON.parse(body);
      var name = user_data.name;
      var public_repo = user_data.public_repos;
      req.session.name = name;
      req.session.username = user_data.login;
      con.query(
        "SELECT * FROM users WHERE username = '" + req.session.username + "'",
        function (err, result, fields) {
          if (err) throw err;
          if (result != "") {

            req.session.type = result[0].type;
            req.session.email = result[0].email;
            res.redirect("/");
          } else {
            res.render("gitUser", {
              name: name,
              public_repo: public_repo
            })
          }
        });
    }
  );
});


// GITHUB USER DETAILS
app.post("/add/user/details", (req, res) => {
  var name = req.session.name;
  if (name == null) {
    req.flash("UserFailed", "Complete your github profile first.");
    res.redirect('/')
  } else {
    var email = req.body.email;
    var type = req.body.type;
    req.session.email = email;
    req.session.type = type;
    var profession = req.body.profession;
    var username = req.session.username;
    con.query(
      "SELECT COUNT(*) AS cnt FROM users WHERE email = ? ",
      email,
      function (err, data) {
        if (err) {
          console.log(err);
        } else {
          if (data[0].cnt > 0) {
            req.flash("UserFailed", "User already exist,please login");
            res.redirect("/");
          } else {
            var secret_id = crypto.randomBytes(10).toString('hex');
            var sql =
              "INSERT INTO users (email, type, secret_id, name, username, profession) VALUES ('" +
              email +
              "', '" +
              type +
              "', '" + secret_id + "', '" + name + "', '" + username + "', '" + profession + "')";
            con.query(sql, function (err, result) {
              if (err);
            });
            req.flash(
              "UserCreated",
              "Account Created Successfully,check your mail for activation link(Don't forget to check your spam folder)"
            );

            var mailOptions = {
              from: "codeleat@gmail.com",
              to: email,
              subject: "LEATCODE",
              text: "Welcome to LEATCODE",
              template: 'signup',
              context: {
                email: email
              }
            };
            transporter.sendMail(mailOptions, function (error, info) {
              if (error) {
                console.log(error);
              } else {
                //console.log('Email sent: ' + info.response);
              }
            });
            res.redirect("/");
          }
        }
      }
    );
  }
});

//ABOUT US
app.get("/about", function (req, res) {
  res.render("about", {
    email: req.session.email
  })
})

// PAGES AFTER USER IS LOGGED IN
//NORMAL USERS
app.get("/courses", function (req, res) {
  con.query(
    "SELECT * FROM courses WHERE id NOT IN(SELECT lesson_id FROM subscriptions WHERE user_email = '" +
    req.session.email +
    "')",
    function (err, result, fields) {
      if (err) throw err;
      res.render("courses", {
        email: req.session.email,
        courses: result,
        courseSubscriptions: req.flash("courseSubscriptions")
      });
    }
  );
});

// VIEW USER
app.get('/user/profile/:value', function (req, res) {
  con.query(
    "SELECT * FROM users WHERE email = '" + req.params.value + "'",
    function (err, result, fields) {
      con.query(
        "SELECT * FROM courses WHERE email = '" + req.params.value + "'",
        function (err, courses_result, fields) {
          if (err) throw err;
          res.render("user_profile", {
            email: req.session.email,
            user: result,
            courses: courses_result
          });
        }
      );
    });
});

// USER COURSES
app.get("/mycourses", function (req, res) {
  con.query(
    "SELECT * FROM courses WHERE id IN(SELECT lesson_id FROM subscriptions WHERE user_email = '" +
    req.session.email +
    "')",
    function (err, result, fields) {
      if (err) throw err;
      res.render("my_courses", {
        email: req.session.email,
        courses: result,
        courseSubscriptions: req.flash("courseSubscriptions")
      });
    }
  );
});

app.get("/manage/usercourse/details/:value", function (req, res) {
  con.query(
    "SELECT * FROM courses WHERE id = '" + req.params.value + "'",
    function (err, result, fields) {
      if (err) throw err;
      con.query(
        "SELECT * FROM lessons WHERE course_id = '" + result[0].id + "'",
        function (err, data, fields) {
          if (err) throw err;
          res.render("mycourseDetails", {
            email: req.session.email,
            details: result,
            lessons: data
          });
        }
      );
    }
  );
});

//COURSE SUBSCRIPTION
app.post("/form/subscribe", (req, res) => {
  var email = req.session.email;
  var course_id = req.body.course_id;
  con.query(
    "SELECT * FROM subscriptions WHERE user_email = '" +
    req.session.email +
    "' AND lesson_id = '" +
    course_id +
    "'",
    function (error, results, fields) {
      if (results.length > 0) {
        req.flash(
          "courseSubscriptions",
          "You are already subscribed to this course"
        );
        res.redirect("/courses");
      } else {
        var sql =
          "INSERT INTO subscriptions (lesson_id, user_email) VALUES ('" +
          course_id +
          "', '" +
          email +
          "')";
        con.query(sql, function (err, result) {
          if (err);
        });
        req.flash(
          "CourseSubscriptions",
          "New Course has been added Succesfully"
        );
        var mailOptions = {
          from: 'codeleat@gmail.com',
          to: email,
          subject: 'New Subscription',
          text: 'Hey you just subscribed to a new course'
        };

        transporter.sendMail(mailOptions, function (error, info) {
          if (error) {
            console.log(error);
          } else {
            console.log('Email sent: ' + info.response);
          }
        });
        res.redirect("/");
      }
    }
  );
});

app.post("/form/unsubscribe", (req, res) => {
  var email = req.session.email;
  var course_id = req.body.course_id;
  con.query(
    "SELECT * FROM subscriptions WHERE user_email = '" +
    req.session.email +
    "' AND lesson_id = '" +
    course_id +
    "'",
    function (error, results, fields) {
      if (results.length > 0) {
        var sql =
          "DELETE FROM subscriptions WHERE lesson_id = '" +
          course_id +
          "' AND user_email =  '" +
          email +
          "'";
        con.query(sql, function (err, result) {
          if (err);
        });
        req.flash(
          "CourseSubscriptions",
          "New Course has been added Succesfully"
        );
        res.redirect("/");
      } else {
        req.flash(
          "courseSubscriptions",
          "You are already subscribed to this course"
        );
        res.redirect("/courses");
      }
    }
  );
});

// Course Details
app.get("/course/details/:value", function (req, res) {
  con.query(
    "SELECT * FROM courses WHERE id = '" + req.params.value + "'",
    function (err, result, fields) {
      if (err) throw err;
      res.render("details", {
        email: req.session.email,
        details: result
      });
    }
  );
});

// LESSON DETAILS
app.get("/view/answer/:value", function (req, res) {
  con.query(
    "SELECT * FROM lessons WHERE id = '" + req.params.value + "'",
    function (err, result, fields) {
      if (err) throw err;
      res.render("view_answer", {
        email: req.session.email,
        details: result
      });
    }
  );
});

// PROFESSORS

// NEW COURSE
app.post("/form/new/course", (req, res) => {
  var email = req.session.email;
  var title = req.body.title;
  var lesson = req.body.lesson;
  var sql =
    "INSERT INTO courses (title, email, lesson) VALUES ('" +
    title +
    "', '" +
    email +
    "', '" +
    lesson +
    "')";
  con.query(sql, function (err, result) {
    if (err);
  });
  req.flash("CourseAdded", "New Course has been added Succesfully");
  res.redirect("/");
});

//ADD LESSON
app.post("/add/lesson", (req, res) => {
  var email = req.session.email;
  var lesson = req.body.lesson;
  var course_title = req.body.course_title;
  var title = req.body.title;
  var course_id = req.body.course_id;
  var sql =
    "INSERT INTO lessons (course_id, lesson, title, course_title) VALUES ('" +
    course_id +
    "', '" +
    lesson +
    "', '" + title + "', '" + course_title + "')";
  con.query(sql, function (err, result) {
    if (err);
  });
  req.flash("CourseAdded", "New lesson has been added Succesfully");
  res.redirect("back");
});

//QUILL TEXT EDITOR
app.get("/editor", function (req, res) {
  res.render("editor");
});

//Manage Course
app.get("/manage/course/details/:value", function (req, res) {
  con.query(
    "SELECT * FROM courses WHERE id = '" +
    req.params.value +
    "' AND email = '" +
    req.session.email +
    "'",
    function (err, result, fields) {
      if (err) throw err;
      con.query(
        "SELECT * FROM lessons WHERE course_id = '" + result[0].id + "'",
        function (err, data, fields) {
          if (err) throw err;
          res.render("adminCourseDetails", {
            email: req.session.email,
            details: result,
            lessons: data
          });
        }
      );
    }
  );
});

//Download App
app.get('/download/app', function (req, res) {
  const file = `public/LEATCODE.apk`;
  res.download(file); // Set disposition and send it.
});

//LOGOUT
app.get("/logout", function (req, res) {
  req.session.destroy();
  res.redirect("/");
});

//ERROR 404
app.get("*", function (req, res) {
  res.render("404");
});

app.listen(port, () => console.log(`Elearn app listening on port ${port}!`));
